#######################################
# BASE IMAGE
#######################################
FROM php:7.4-fpm-alpine as base

WORKDIR /var/www

# Install dependencies
RUN set -xe \
    && apk add --no-cache bash icu-dev libgd libjpeg libpng-dev libzip-dev postgresql-dev \
    && docker-php-ext-install gd intl opcache pcntl pdo_pgsql zip

COPY docker/app/entrypoint.sh /usr/local/bin/php-entrypoint

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY docker/app/custom.ini $PHP_INI_DIR/conf.d/

CMD ["/usr/local/bin/php-entrypoint"]

#######################################
# COMPOSER
#######################################
FROM base as build

COPY --from=composer:1 /usr/bin/composer /usr/local/bin/composer

RUN rm -rf /var/www && mkdir /var/www
WORKDIR /var/www

COPY app/composer.* /var/www/

ARG APP_ENV=prod

# Some packages require git to install
RUN set -xe \
    && apk add --no-cache git

RUN set -xe \
    && if [ "$APP_ENV" = "prod" ]; then export ARGS="--no-dev"; fi \
    && composer install --prefer-dist --no-scripts --no-progress --no-suggest --no-interaction $ARGS

# Remove assets from app image except those required for app function
COPY app/. /var/www
RUN set -xe \
    && rm -R /var/www/assets/*
COPY app/assets/static/map /var/www/assets/static/map

RUN composer dump-autoload --classmap-authoritative

#######################################
# DATA DOCS
#######################################
FROM php:7.4-cli-alpine as docs

COPY --from=composer:1 /usr/bin/composer /usr/local/bin/composer

RUN rm -rf /var/www && mkdir /var/www
WORKDIR /var/www

COPY doc /var/www/doc
COPY app/resources/schema /var/www/app/resources/schema
WORKDIR /var/www/doc

# Some packages require git to install
RUN set -xe \
    && apk add --no-cache git

RUN composer install --prefer-dist --no-scripts --no-progress --no-suggest --no-interaction --no-dev

RUN vendor/bin/daux generate --destination=/var/www/public

#######################################
# ASSETS
#######################################
FROM node:12-alpine as webpack

ARG APP_ENV=prod

RUN apk add --no-cache git

RUN rm -rf /var/www && mkdir /var/www
WORKDIR /var/www

COPY app/public app/yarn.lock app/package.json app/webpack.config.js app/postcss.config.js /var/www/
COPY app/assets /var/www/assets
# Some assets come from PHP vendors
COPY --from=build /var/www/vendor/ /var/www/vendor/

# Some packages require git to install
RUN set -xe \
    && apk add --no-cache git

RUN set -xe \
    && yarn install --non-interactive  --frozen-lockfile $ARGS

RUN set -xe \
    && mkdir -p public/build \
    && if [ "$APP_ENV" = "prod" ]; then export SCRIPT="build"; else export SCRIPT="dev"; fi \
    && yarn run $SCRIPT

# Cleanup sources to reduce image size
RUN set -xe \
    && rm -R /var/www/assets

#######################################
# APPLICATION
#######################################
FROM base as app

ARG APP_ENV=prod
ARG APP_DEBUG=0
ARG BUILD_NUMBER=debug

ENV APP_ENV $APP_ENV
ENV APP_DEBUG $APP_DEBUG
ENV SENTRY_DSN $SENTRY_DSN
ENV BUILD_NUMBER $BUILD_NUMBER

COPY --from=build /var/www/ /var/www/
COPY --from=webpack /var/www/public/build/manifest.json /var/www/public/build/manifest.json
COPY --from=webpack /var/www/public/build/entrypoints.json /var/www/public/build/entrypoints.json

RUN mkdir -p var/cache \
    && chown -R www-data:www-data var

RUN IDE=none bin/console assets:install
RUN chown -R www-data:www-data var

#######################################
# WEB SERVER
#######################################
FROM nginx:stable-alpine as web

ARG NGINX_BACKEND_HOST=app
ENV NGINX_BACKEND_HOST $NGINX_BACKEND_HOST

WORKDIR /var/www/public

COPY docker/web/entrypoint.sh /usr/local/bin/nginx-entrypoint
COPY docker/web/default.conf /etc/nginx/conf.d/default.conf.tmpl

COPY --from=webpack /var/www/public/build /var/www/public/build
COPY --from=app /var/www/public/bundles /var/www/public/bundles
COPY --from=docs /var/www/public /var/www/public/doc
COPY app/resources/schema /var/www/public/data/schema

CMD ["/usr/local/bin/nginx-entrypoint"]

#######################################
# APP DEVELOPMENT SUPPORT
#######################################
FROM app as app_dev

COPY --from=build /usr/local/bin/composer /usr/local/bin/composer

RUN set -xe \
    && apk add --no-cache $PHPIZE_DEPS \
    && pecl install xdebug-2.9.5 \
    && docker-php-ext-enable xdebug

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

#######################################
# WEB DEVELOPMENT SUPPORT
#######################################
FROM web as web_dev
